﻿Imports System.Runtime.Serialization
Imports System.IO
Imports System.Reflection


'http://stackoverflow.com/questions/12566946/serialize-a-generic-object-to-ini-text-file-implementing-a-custom-iformatter

Public Class IniFormatter(Of T)
    Implements IFormatter




    Sub New()
        Context = New StreamingContext(StreamingContextStates.All)
    End Sub



    Public Property Binder As SerializationBinder Implements IFormatter.Binder

    Public Property Context As StreamingContext Implements IFormatter.Context

    Public Function Deserialize(serializationStream As IO.Stream) As Object Implements IFormatter.Deserialize

    End Function

    'Public Sub Serialize(serializationStream As IO.Stream, graph As Object) Implements IFormatter.Serialize
    '    Dim sw As TextWriter = New StreamWriter(serializationStream)



    '    Dim flags As BindingFlags = BindingFlags.Instance _
    '                   Or BindingFlags.Public _
    '                  Or BindingFlags.DeclaredOnly _
    '                   Or BindingFlags.Static


    '    Dim t As Type = graph.GetType


    '    Dim pr As PropertyInfo() = t.GetProperties(flags)
    '    For Each m As PropertyInfo In pr


    '        Dim value As Object = GetPropertyValue(graph, m.Name)

    '        If m.PropertyType.IsPrimitive Then
    '            sw.WriteLine("{0}={1}", m.Name, value)

    '        ElseIf m.PropertyType.IsGenericType Then

    '            sw.WriteLine("{0}= Collection", m.Name, value)
    '            Serialize(serializationStream, m)

    '            sw.WriteLine("{0}= Collection", m.Name, value)

    '        Else

    '            Select Case m.PropertyType
    '                Case GetType(String)
    '                    sw.WriteLine("{0}={1}", m.Name, value)

    '            End Select







    '        End If


    '    Next


    '    sw.Flush()


    'End Sub
    'Public Function GetPropertyValue(ByVal obj As Object, ByVal PropName As String) As Object
    '    Dim objType As Type = obj.GetType()
    '    Dim pInfo As System.Reflection.PropertyInfo = objType.GetProperty(PropName)
    '    Dim PropValue As Object = pInfo.GetValue(obj, Reflection.BindingFlags.GetProperty, Nothing, Nothing, Nothing)
    '    Return PropValue
    'End Function
    Public Property SurrogateSelector As ISurrogateSelector Implements IFormatter.SurrogateSelector


    Public Sub Serialize(serializationStream As Stream, graph As Object) Implements IFormatter.Serialize

        Dim sw As TextWriter = New StreamWriter(serializationStream)

        Dim IsISerializable As Boolean = TypeOf graph Is ISerializable
        Dim objectType As Type
        objectType = GetType(T)


        If IsISerializable Then
            Dim serializable As ISerializable = graph
            Dim info As SerializationInfo
            info = New SerializationInfo(objectType, New FormatterConverter())
            serializable.GetObjectData(info, Context)

            If Not info Is Nothing Then

                For Each member As SerializationEntry In info

                    If member.ObjectType = GetType(String) Or member.GetType.IsPrimitive Then
                        sw.WriteLine("{0}={1}", member.Name, member.Value.ToString)
                    Else
                        'completar para tipos complejos
                    End If

                Next
            End If
        Else
            'usar Reflexion
            
        End If

        sw.Close()


   




    End Sub
End Class
