﻿Imports System.Xml.Schema
Imports System.Xml

Public Class ValidadorXML

    Shared errors As Boolean = False
    Shared schemas As XmlSchemaSet
    Shared Sub XSDErrors(ByVal o As Object, ByVal e As ValidationEventArgs)
        Console.WriteLine("{0}", e.Message)
        errors = True
    End Sub


    Shared Sub Validar()
        Dim xsdMarkup As XElement = XElement.Load("XML.Addons\schema.xml", LoadOptions.None)
        schemas = New XmlSchemaSet()
        schemas.Add("", xsdMarkup.CreateReader)

        Dim doc1 As XDocument = XDocument.Load("XML.Addons\Doc1.xml", LoadOptions.None)

        Dim doc2 As XDocument = XDocument.Load("XML.Addons\Doc2.xml", LoadOptions.None)


        errors = False
        doc1.Validate(schemas, AddressOf XSDErrors)
        MsgBox(String.Format("doc1 {0}", IIf(errors = True, "es invalido", "es valido")))

        errors = False
        doc2.Validate(schemas, AddressOf XSDErrors)
        MsgBox(String.Format("doc2 {0}", IIf(errors = True, "es invalido", "es valido")))
    End Sub

End Class





