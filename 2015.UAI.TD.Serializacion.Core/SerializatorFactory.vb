﻿Public Class SerializatorFactory
    Shared Function GetSerializator(Of T)(ext As String) As AbstractSerializator


        Select Case ext
            Case ".json"
                Return New JsonSerializator(Of T)
            Case ".xml"
                Return New XmlSerializator(Of T)
            Case ".dat"
                Return New BinarySerializator(Of T)
            Case ".soap"
                Return New SOAPSerializator(Of T)

            Case Else
                Return Nothing
        End Select

    End Function
End Class
