﻿Imports Newtonsoft.Json
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Xml.Schema

Public Class JsonSerializator(Of T)
    Inherits AbstractSerializator


    Public Overrides Function Deserializar(str As Stream) As Object

        Dim serializer As New JsonSerializer()
        Dim tr As TextReader = New StreamReader(str)

        Dim o As Object = serializer.Deserialize(tr, GetType(T))
        tr.Close()
        Return o

    End Function

    Public Overrides Function Serializar(que As Object)

        Dim path As String
        fs = FileStreamManager.Instance.CreateFile("json")
        path = fs.Name
        writer = New StreamWriter(fs)

        Dim serializer As New JsonSerializer()

        Using writer
            serializer.Serialize(writer, que)

        End Using
        writer.Close()

        fs.Close()
        Return path



    End Function
End Class
