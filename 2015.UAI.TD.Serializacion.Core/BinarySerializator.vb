﻿Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization

Public Class BinarySerializator(Of T)
    Inherits AbstractSerializator

    Public Overrides Function Deserializar(str As Stream) As Object
        Dim formatter As New BinaryFormatter
        Return formatter.Deserialize(str)



    End Function

    Public Overrides Function Serializar(que As Object)
        fs = FileStreamManager.Instance.CreateFile("dat")
        'writer = New StreamWriter(fs)
        Dim formatter As New BinaryFormatter
        formatter.Serialize(fs, que)
        'writer.Close()
        fs.Close()
        Return fs.Name
    End Function
End Class
