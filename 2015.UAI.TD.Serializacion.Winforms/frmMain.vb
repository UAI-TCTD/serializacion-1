﻿Imports _2015.UAI.TD.Serializacion.BE
Imports _2015.UAI.TD.Serializacion.Core
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Xml.Serialization


Public Class frmMain
    Public serializador As New Serializador()
    Public _personas As New List(Of Persona)
    Public _personas2 As List(Of Persona)

    Private Sub cargarArchivo(Of T)()
        Dim myStream As Stream = Nothing
        Dim openFileDialog1 As New OpenFileDialog()

        'openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "serialized files (*.dat, *.xml, *.json)|*.dat; *.xml; *.json"
        openFileDialog1.FilterIndex = 1
        'openFileDialog1.RestoreDirectory = True

        If (openFileDialog1.ShowDialog() = DialogResult.OK) Then

            Try

                Dim ext As String = Path.GetExtension(openFileDialog1.FileName)

                myStream = openFileDialog1.OpenFile()
                If (myStream IsNot Nothing) Then
                    ' Insert code to read the stream here.

                    Dim serializator As AbstractSerializator = SerializatorFactory.GetSerializator(Of T)(ext)
                    Dim p As Object = serializator.Deserializar(myStream)
                    _personas2 = New List(Of Persona)
                    If TypeOf (p) Is Persona Then
                        _personas2.Add(p)
                    Else
                        _personas2 = p
                    End If

                End If

                Me.lstPersonas2.DataSource = _personas2
            Catch Ex As Exception
                MessageBox.Show("Imposible deserializar: " & Ex.Message)
            Finally

                If (myStream IsNot Nothing) Then
                    myStream.Close()
                End If
            End Try
        End If

    End Sub


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _personas.Add(New Persona("26441334", "Roberto", "Carlos"))
        _personas.Add(New Persona("18112133", "Joaquin", "Sabina"))


        Me.lstPersonas.DataSource = _personas

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles cmdSerializacionBinaria.Click

        If (optTodo.Checked) Then
            serializador.Serializar(New BinarySerializator(Of List(Of Persona)), _personas)
        Else
            serializador.Serializar(New BinarySerializator(Of Persona), Me.lstPersonas.SelectedItem)
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles cmdSerializacionXML.Click

        If (optTodo.Checked) Then
            serializador.Serializar(New XmlSerializator(Of List(Of Persona)), _personas)
        Else
            serializador.Serializar(New XmlSerializator(Of Persona), Me.lstPersonas.SelectedItem)
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles cmdSerializacionJson.Click

        If (optTodo.Checked) Then

            serializador.Serializar(New JsonSerializator(Of List(Of Persona)), _personas)
        Else

            serializador.Serializar(New JsonSerializator(Of Persona), Me.lstPersonas.SelectedItem)
        End If


    End Sub


    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles cmdSerializacionCustom.Click


        If (optTodo.Checked) Then

            serializador.Serializar(New IniSerializator(Of List(Of Persona)), _personas)
        Else

            serializador.Serializar(New IniSerializator(Of Persona), Me.lstPersonas.SelectedItem)
        End If




    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles cmdDeserializaPersona.Click

        cargarArchivo(Of Persona)()


    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles cmdDeserializaArrayPersona.Click
        cargarArchivo(Of List(Of Persona))()
    End Sub

    Private Sub cmdValidar_Click(sender As Object, e As EventArgs) Handles cmdValidar.Click
        ValidadorXML.Validar()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        If (optTodo.Checked) Then

            serializador.Serializar(New SOAPSerializator(Of List(Of Persona)), _personas)
        Else

            serializador.Serializar(New SOAPSerializator(Of Persona), Me.lstPersonas.SelectedItem)
        End If
    End Sub
End Class
